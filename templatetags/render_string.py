from django import template
from django.template import RequestContext, Template, Context
from django.template.loader import render_to_string
register = template.Library()

@register.simple_tag(takes_context=True)
def render_string(context, string):
    context_dict = {}
    for d in context:
        context_dict.update(d)
    t = Template(string)
    c = Context(context_dict)
    return t.render(c)